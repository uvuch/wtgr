require 'zip'

class GroundRoutesController < ApplicationController
  def index
  end
  
  def import
    @airport = Airport.new(params[:file])
    
    @airport.ground_routes.each do |route|
      content = render_to_string(partial: "ground_routes/out.txt",
                                 locals: {route: route})
      filename = Rails.root.join('public', 'files', route.filename)
      
      file = File.open(filename, 'w')
      file.write(content) unless file.nil?
      file.close unless file.nil?
      
    end  
      
    if @airport.ground_routes.any?
      zipfilename = 'ground_routes_' + Time.now.to_i.to_s + '.zip'
      #temp_file = Tempfile.new(zipfilename)
      zipfilepath = Rails.root.join('public', 'zip', zipfilename)
      input_filenames = @airport.ground_routes.map { |route| route.filename }

      Zip::File.open(zipfilepath, Zip::File::CREATE) do |zipfile|
        input_filenames.each do |filename|
          # Two arguments:
          # - The name of the file as it will appear in the archive
          # - The original file, including the path to find it
          filepath = Rails.root.join('public', 'files', filename)
          zipfile.add(filename, filepath)
        end
        zipfile.get_output_stream("myFile") { |os| os.write "myFile contains just this" }
      end
      #  temp_file.close
      #  temp_file.unlink
      
      zip_data = File.read(zipfilepath)
      return send_data(zip_data, :type => 'application/zip', :filename => zipfilename)
    end  
    

    render :index, content_type: 'text/html'
  end
end
