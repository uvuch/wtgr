# point name
# speed | heading | description | cid | runway/taxiway name
#  -3         -1        taxiway     0        park          
#  20.0       -1        taxiway     0         D
#  20.0       -1         16L        1      16L/34R
#  20         -1         16C        2        toff
#
# -3|-1|taxiway|0|park -- the same as below
#
# speed | heading | description | cid | runway/taxiway name
#  -3       -1       taxiway       0    park               
class RoutePoint
    attr_reader :latitude
    attr_reader :longtitude
    attr_reader :speed
    attr_reader :heading
    attr_reader :description
    attr_reader :cid
    attr_reader :rwy_twy_name
    attr_reader :name
    
    def initialize(node)
        @object = node
        hierarchy = node.css('hierarchy')
        point = node.css('point')

        @name = hierarchy.attribute('name').to_s
        @latitude = point.attribute('latitude').to_s
        @longtitude = point.attribute('longitude').to_s
        
        parse_name
    end
    
    private
    
    def parse_name
        match = name.split('|')
        @speed = match[0]
        @heading = match[1] if match[1]
        @description = match[2] if match[2]
        @cid = match[3] if match[3]
        @rwy_twy_name = match[4] if match[4]
    end
end