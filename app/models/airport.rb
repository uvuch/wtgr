class Airport
    attr_reader :ground_routes
    
    def initialize(file)
        @document = Nokogiri::HTML(file)
        @ground_routes = []
        read_routes
    end
    
    private
    
    def read_routes
        @document.xpath("//hierarchy").each do |xmlnode|
            next unless xmlnode.attribute('name')
                               .to_s.match(/^(dep|arr)\p{Blank}*:/i)
            @ground_routes << GroundRoute.new(xmlnode, @document)
        end
    end
end