# route name
# <DEP|ARR>:<RWY> spot:<main|nose>
#                 <number|name|type>:<value> [<operator:value>]
# where value can be comma separated list of values (no spaces).
# keywords are case insensitive, so eny of DEP,dep,DeP and so on works.
# Examples:
# dep:24 type:1,5,7 operator:JFG,KNS,KSH
# arr:15 number:JS165
# DEP:27L name:A319 operator:AAN
#

class GroundRoute
    attr_reader :name
    attr_reader :runway
    attr_reader :spot
    attr_reader :tail_number
    attr_reader :plane_name
    attr_reader :aircraft_type
    attr_reader :operator
    attr_reader :stand
    attr_reader :filename
    attr_reader :points
    
    def initialize(node, document)
        @name = node.attribute('name').to_s
        parse_name
        @object = node.parent
        @document = document
        read_points @object.css('child')
    end
    
    private
    
    def read_points(node_references)
        @points = []
        node_references.each do |ref|
            id = ref.attribute('id').to_s
            node = @document.css('object[id="' + id + '"]').first
            @points << RoutePoint.new(node)
        end
    end
    
    def parse_name
        @runway = parse_runway
        @spot = parse_spot
        @tail_number = parse_tail_number
        @plane_name = parse_plane_name unless @tail_number
        @aircraft_type = parse_aircraft_type unless @tail_number || @plane_name
        @operator = parse_operators unless @tail_number
        @stand = parse_stand_name
        @filename = parse_file_name
    end
    
    def parse_runway
       match = @name.match(/^(dep|arr)\p{Blank}*:\p{Blank}*(\p{Digit}+[LRC]?)/i)
       (match.present? && match[2]).present? ? match[2].upcase : ''
    end
    
    def parse_spot
        match = @name.match(/spot\p{Blank}*:\p{Blank}*nose/i)
        match.present? ? 'NOSEWHEEL' : 'MAINWHEEL'
    end
    
    def parse_tail_number
        match = @name.match(/number\p{Blank}*:\p{Blank}*(\S+)/i)
        match[1] if match.present? && match[1].present?
    end
    
    def parse_plane_name
        match = @name.match(/name\p{Blank}*:\p{Blank}*(\S+)/i)
        match[1] if match.present? && match[1].present?
    end
    
    def parse_aircraft_type
        match = @name.match(/type\p{Blank}*:\p{Blank}*(\S+)/i)
        types = '0 1 2 3 4 5 6 7 8' # all types
        types = match[1].gsub(',', ' ') if match && match[1]
    end
    
    def parse_operators
        match = @name.match(/operator\p{Blank}*:\p{Blank}*(\S+)/i)
        match[1].gsub(',', ' ').upcase if match && match[1]
    end
    
    def parse_stand_name
        match = @name.match(/stand\p{Blank}*:\p{Blank}*(\S+)/i)
        match[1] if match.present? && match[1].present?
    end
    
    def parse_file_name
        filename = "DEP#{@runway}"
        filename += "_#{@stand}" if @stand
        filename += "_#{@tail_number}" if @tail_number
        filename += "_#{@plane_name}" if @plane_name
        filename += "_#{@aircraft_type.gsub(' ', '')}" if @aircraft_type
        filename + '.txt'
    end
end